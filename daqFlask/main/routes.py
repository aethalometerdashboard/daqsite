from flask import render_template, Blueprint, request, redirect, url_for, session, flash, send_file, current_app
from daqFlask.main.forms import LocationForm
from daqFlask.scripts.generator import generator
from datetime import datetime, timedelta

import os
import shutil
from shutil import copyfile

import io
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

main = Blueprint('main', __name__)


def initiate_directory(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)


@main.context_processor
def inject_now():
    return {'now': datetime.utcnow()}


@main.route("/getzip")
def get_zip():
    root_path = os.path.dirname(current_app.instance_path) + '/'
    static_dir = os.path.join(root_path, 'daqFlask/static/')
    data_dir = os.path.join(root_path, 'data/')

    user = session.get('user', None)

    holder_dir = os.path.join(data_dir, 'holder/')
    user_dir = os.path.join(holder_dir, user)

    bdays_file = os.path.join(data_dir, 'daytype/forecast.action.log.csv')

    newPath = shutil.copy(bdays_file, user_dir)

    zip_dest = os.path.join(static_dir, 'downloads/')

    initiate_directory(zip_dest)

    zip_file = zip_dest + 'AirData' + user

    shutil.make_archive(root_dir=user_dir,
                        format='zip', base_name=zip_file)

    zip_file = zip_file + '.zip'

    return send_file(zip_file,
                     mimetype='zip',
                     attachment_filename='AirData.zip',
                     as_attachment=True)


@main.route("/", methods=['GET', 'POST'])
@main.route("/home", methods=['GET', 'POST'])
def home():

    root_path = os.path.dirname(current_app.instance_path) + '/'

    form = LocationForm()

    if form.validate_on_submit():

        stationlist = []

        request.form.getlist("")

        time = datetime.now().strftime('%M%S%f')[:-4]
        todaysdate = datetime.now().strftime("%Y-%m-%d")

        if(form.BV_checkbox.data):
            stationlist.append('bv')

        if(form.LN_checkbox.data):
            stationlist.append('ln')

        if(form.SM_checkbox.data):
            stationlist.append('sm')

        if(form.HD_checkbox.data):
            HD_indicator = True
        else:
            HD_indicator = False

        if(form.SP_checkbox.data):
            SP_indicator = True
        else:
            SP_indicator = False

        time = datetime.now().strftime('%M%S%f')[:-4]
        todaysdate = datetime.now().strftime("%Y-%m-%d")

        startDate = form.StartDate.data
        endDate = form.EndDate.data

        startDate = startDate.strftime('%Y-%m-%d')
        endDate = endDate.strftime('%Y-%m-%d')

        edge_date = '2017-07-18'

        user = todaysdate + time
        session['user'] = user

        if (todaysdate <= startDate):
            flash('Start date must be before today', 'warning')
            return render_template('home.html', form=form)

        if (todaysdate < endDate):
            flash('End date cannot be after today', 'warning')
            return render_template('home.html', form=form)

        if (endDate <= startDate):
            flash('Start date must be before end date', 'warning')
            return render_template('home.html', form=form)

        if not stationlist == []:
            if (startDate < edge_date):
                flash(
                    '07/18/2017 is the earliest date for delta-carbon data.', 'warning')
                return render_template('home.html', form=form)

        if (stationlist == []):

            if(HD_indicator):
                file_names = generator(
                    startDate, endDate, stationlist, user, HD_indicator, SP_indicator, root_path)
                session['id'] = file_names
                return redirect(url_for('main.display', startDate=startDate, endDate=endDate))
            else:
                flash(
                    "You must select at least one location, or heat-deficit", "warning")
                form = LocationForm()
                return render_template('home.html', form=form)
        else:
            file_names = generator(
                startDate, endDate, stationlist, user, HD_indicator, SP_indicator, root_path)
            session['id'] = file_names
            return redirect(url_for('main.display', startDate=startDate, endDate=endDate))

    if form.errors:
        # print(form.errors)
        flash("You must select a valid date range, in this format: 'YYYY-MM-DD'", "warning")
        form = LocationForm()
        return render_template('home.html', form=form)
    return render_template('home.html', form=form)


@main.route("/about")
def about():
    return render_template('about.html')


@main.route("/display/<string:startDate>/<string:endDate>", methods=['GET', 'POST'])
def display(startDate, endDate):

    root_path = os.path.dirname(current_app.instance_path) + '/'

    form = LocationForm()

    if form.validate_on_submit():

        stationlist = []

        request.form.getlist("")

        if(form.BV_checkbox.data):
            stationlist.append('bv')

        if(form.LN_checkbox.data):
            stationlist.append('ln')

        if(form.SM_checkbox.data):
            stationlist.append('sm')

        if(form.HD_checkbox.data):
            HD_indicator = True
        else:
            HD_indicator = False

        if(form.SP_checkbox.data):
            SP_indicator = True
        else:
            SP_indicator = False

        startDate = form.StartDate.data
        endDate = form.EndDate.data

        startDate = startDate.strftime('%Y-%m-%d')
        endDate = endDate.strftime('%Y-%m-%d')

        edge_date = '2017-07-18'

        time = datetime.now().strftime('%M%S%f')[:-4]
        todaysdate = datetime.now().strftime("%Y-%m-%d")

        user = todaysdate + time
        session['user'] = user

        if (todaysdate <= startDate):
            flash('Start date must be before today', 'warning')
            return render_template('home.html', form=form)

        if (todaysdate < endDate):
            flash('End date cannot be after today', 'warning')
            return render_template('home.html', form=form)

        if (endDate <= startDate):
            flash('Start date must be before end date', 'warning')
            return render_template('display.html', form=form)

        if not stationlist == []:
            if (startDate < edge_date):
                flash(
                    '07/18/2017 is the earliest date for delta-carbon data.', 'warning')
                return render_template('display.html', form=form)

        if (stationlist == []):

            if(HD_indicator):
                file_names = generator(
                    startDate, endDate, stationlist, user, HD_indicator, SP_indicator, root_path)
                session['id'] = file_names
                return redirect(url_for('main.display', startDate=startDate, endDate=endDate))
            else:
                flash(
                    "You must select at least one location, or heat-deficit", "warning")
                form = LocationForm()
                return render_template('display.html', form=form)
        else:
            file_names = generator(
                startDate, endDate, stationlist, user, HD_indicator, SP_indicator, root_path)
            session['id'] = file_names
            return redirect(url_for('main.display', startDate=startDate, endDate=endDate))

    if form.errors:
        # print(form.errors)
        flash("You must select a valid date range, in this format: 'YYYY-MM-DD'", "warning")
        form = LocationForm()
        return render_template('display.html', form=form)
    return render_template('display.html', form=form)
