from flask_wtf import FlaskForm
from wtforms import BooleanField, SubmitField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired


class LocationForm(FlaskForm):

    StartDate = DateField('Start:', format='%Y-%m-%d',
                          validators=[DataRequired()])
    EndDate = DateField('End:', format='%Y-%m-%d',
                        validators=[DataRequired()])
    HD_checkbox = BooleanField('Heat Deficit On')
    SP_checkbox = BooleanField('Generate Separate Plots')
    SM_checkbox = BooleanField('Smithfield')
    BV_checkbox = BooleanField('Bountiful')
    RP_checkbox = BooleanField('Rose Park')
    LN_checkbox = BooleanField('Lindon')
    submit = SubmitField('Submit')
