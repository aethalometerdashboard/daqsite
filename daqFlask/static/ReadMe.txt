ReadMe.txt for DAQ-UU Aethalometer--Heat-Deficit Data Download
## ----------------------------------------------------------------------------

Aethalometer data is collected from the Utah Division of Air Quality.
Data collected from three monitoring stations: Bountiful, Lindon, Smithfield.
Data collected is hourly averaged.
Data sources (web) are given below (note variables for Year, Month, Day):
airmonitoring.utah.gov/dataarchive/blackcarbon/bv/bv-1hr-ae33-7-YYYY-MM-DD.csv
airmonitoring.utah.gov/dataarchive/blackcarbon/ln/ln-1hr-ae33-7-YYYY-MM-DD.csv
airmonitoring.utah.gov/dataarchive/blackcarbon/sm/sm-1hr-ae33-7-YYYY-MM-DD.csv




HeatDeficit data is calculated from rawinsonde readings
Rawinsonde launches from SLC international airport
Rawinsonde data obtained from Wyoming Weather Web via siphon api
Wyoming Weather Web is from University of Wyoming, Dept. of Atmospheric Science
Details on the api can be found at:
https://github.com/Unidata/siphon
https://unidata.github.io/siphon/latest/api/index.html

Direct access to the rawinsonde data using (station number = 72672):
http://weather.uwyo.edu/upperair/sounding.html


## ----- DATAFILE: YYYY-MM-DDxxxxxx.csv ---------------------------------------
Definition of data columns (Aethalometer):

1) Communication Channel (see below)
2) Device ID (see below)
3) Datestamp
4) DateTimeStamp (always in MST)
5) Channel 1 (wavelength: 370 nm)
6) Channel 2 (wavelength: 470 nm)
7) Channel 3 (wavelength: 520 nm)
8) Channel 4 (wavelength: 590 nm)
9) Channel 5 (wavelength: 660 nm)
10) Channel 6 (wavelength: 880 nm)
11) Channel 7 (wavelength: 950 nm)
12) Delta Carbon (ng/m^3)


Aethalometer station comm-channels and device identifiers:

- Bountiful:    COMM == COM6        DEVICE == 0508
- Lindon:       COMM == COM7        DEVICE == 0507
- Smithfield:   COMM == COM4        DEVICE == 0509


## ----- DATAFILE: HDYYYY-MM-DDxxxxxx.csv -------------------------------------
Definition of data columns (heat-deficit):

1) DateTimeStamp (always in MST)
2) Heat-deficit (MJ/m^2)



## ## ----- CONTACT INFORMATION -----------------------------------------------
Kerry Kelly (kerry dot kelly at utah dot edu, 801-585-1414)
University of Utah
Department of Chemical Engineering

