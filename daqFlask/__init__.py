from flask import Flask
from flask_mail import Mail
from daqFlask.config import Config

# For creating session IDs
from datetime import datetime


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    from daqFlask.main.routes import main
    from daqFlask.errors.handlers import errors

    app.register_blueprint(main)
    app.register_blueprint(errors)

    return app
