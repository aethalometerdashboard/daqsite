import os
import json

daqsite_dir = os.getcwd() + '/'
config_path = daqsite_dir + 'etc/config.json'
with open(config_path) as config_file:
    config = json.load(config_file)


class Config:

    SECRET_KEY = config.get('SECRETKEY')
    MAIL_USERNAME = config.get('DAQSENDMAIL')
    MAIL_PASSWORD = config.get('DAQSENDPASS')
