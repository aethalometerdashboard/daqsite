import requests
import urllib3
import os
import csv

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Earliest available dates for each station: Bountiful 20170717, Lindon 20190422, SM 20180904

def downloader(url, outDir, outFile, indicator, error_path):

    path = outDir + outFile
    path = os.path.expanduser(path)

    r = requests.get(url, verify=False)

    if r.status_code == 404:

        if indicator == "aeth":
            errors_filename = error_path + 'errors.csv'

        if indicator == "daytype":
            errors_filename = error_path + 'errors.csv'

        with open(errors_filename, 'a') as f:
            f.write(url)
            f.write(',0\n')
            f.close()
    else:
        with open(path, 'wb') as f:
            f.write(r.content)

    # Retrieve HTTP meta-data
    # print('[INFO]  ' + 'status code: ' + str(r.status_code))
    # print('[INFO]  ' + '  file type: ' + str(r.headers['content-type']))
    # print(r.encoding)
