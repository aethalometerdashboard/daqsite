from rawinsonde import rawinsonde
from datetime import datetime, timedelta
import pandas as pd
import os

# date is expected as a date-time object
# data_path is expected to be '../daqsite/data/'


def heatdef(date, data_path):

    print()
    print()
    print('<<< HeatDef call using date: ' + str(date) + ' >>>')

    #outFile = os.path.expanduser(data_path + '/data/heatDeficit.csv')
    #outFile = os.path.expanduser(data_path + '/data/heatDeficitHistorical.csv')
    #outFileRAW = os.path.expanduser(data_path + '/data/heatDeficitRAWHistorical.csv')

    outFile = os.path.expanduser(data_path + '/heatdeficit/heatDeficit.csv')
    outFileRAW = os.path.expanduser(
        data_path + '/heatdeficit/heatDeficitRAW.csv')

    print('[INFO]  heat-deficit data acquisition begins..')

    # #TASK: need to read the extant csv into a dataframe
    #        doing this will allow us to keep the csv complete and sorted

    # variable declarations

    # print(type(date))

    today = date
    yesterday = today - timedelta(1)
    datePM = today.replace(hour=0, minute=0, second=0, microsecond=0)
    dateAM = yesterday.replace(hour=12, minute=0, second=0, microsecond=0)
    # consequence: these write to the dataframe in reverse order

    #print('datePM', datePM)
    # print(type(datePM))
    #print('dateAM', dateAM)
    # put a conditional here.. (it should surround the rest of this script)
    # if last two lines of outFile are datePM - timedelta(1) and dateAM - timedelta(1)
    # then write to the file

    # call on the rawinsonde function for 00 hour

    print('[INFO]  aquiring data for', dateAM)

    try:
        df12 = rawinsonde(dateAM, data_path)
    except:
        print('[INFO]  Data pull fails for:', dateAM)

    print('[INFO]  aquiring data for', datePM)

    try:
        df00 = rawinsonde(datePM, data_path)
    except:
        print('[INFO]  Data pull fails for:', datePM)

    try:
        if df12 is None:
            print('df12 is none')
            dates = [dateAM - timedelta(hours=7)]
            print('datestamp:', dates[0])
            vals = ['NaN']

            list = {'Dates': dates, 'Vals': vals}

            df = pd.DataFrame(list)
            # print(df)

            print('[INFO]  wrote line for', dates[0])
            with open(outFile, 'a') as f:
                df.to_csv(f, header=False, index=False)
            f.close()

            # put a line here writing NaN to RAW csv
            list = {'Dates': dates, 'na': vals, 'nb': vals,
                    'nc': vals, 'nd': vals, 'ne': vals, 'nf': vals}

            dfRAWNaN = pd.DataFrame(list)
            with open(outFileRAW, 'a') as f:
                dfRAWNaN.to_csv(f, header=False, index=False)
            f.close()
            print('[INFO]  wrote line RAW for', dates[0])
    except:
        print()

    try:

        print('inside the 12 TRY')

        dfRAW = pd.concat([df12], ignore_index=True)
        dfRAW['DATETIME'] = dfRAW['DATETIME'] - pd.Timedelta(hours=7)

        hd12 = df12['heatDefHeight'].sum()
        dateVals = [dateAM - timedelta(hours=7)]
        hdVals = [hd12]
        df = pd.DataFrame(dateVals, columns=['DATETIME'])
        df['HEATDEF'] = hdVals

        print('[INFO]  Writing the following dataframe')
        print(df)

        with open(outFile, 'a') as f:
            df.to_csv(f, header=False, index=False)
        f.close()
        print('[INFO]  wrote line for', dateVals[0])

        print('[INFO]  heat-deficit data loaded for',
              dateVals[0])

        with open(outFileRAW, 'a') as f:
            dfRAW.to_csv(f, header=False, index=False)
        f.close()
        print('[INFO]  wrote line RAW for', dateVals[0])

    except (NameError, AttributeError, ValueError, TypeError) as e:
        print(e)


#####

    try:
        if df00 is None:
            print('df00 is none')
            dates = [datePM - timedelta(hours=7)]
            print('datestamp:', dates[0])
            vals = ['NaN']
            list = {'Dates': dates, 'Vals': vals}

            df = pd.DataFrame(list)
            # print(df)

            print('[INFO]  wrote line for', dates[0])
            with open(outFile, 'a') as f:
                df.to_csv(f, header=False, index=False)
            f.close()

            # put a line here writing NaN to RAW csv

            list = {'Dates': dates, 'na': vals, 'nb': vals,
                    'nc': vals, 'nd': vals, 'ne': vals, 'nf': vals}

            dfRAWNaN = pd.DataFrame(list)
            with open(outFileRAW, 'a') as f:
                dfRAWNaN.to_csv(f, header=False, index=False)
            f.close()
            print('[INFO]  wrote line RAW for', dates[0])
    except:
        print()

    try:
        print('inside the 00 TRY')

        dfRAW = pd.concat([df00], ignore_index=True)
        dfRAW['DATETIME'] = dfRAW['DATETIME'] - pd.Timedelta(hours=7)

        hd00 = df00['heatDefHeight'].sum()
        dateVals = [datePM - timedelta(hours=7)]
        hdVals = [hd00]
        df = pd.DataFrame(dateVals, columns=['DATETIME'])
        df['HEATDEF'] = hdVals

        print('[INFO]  Writing the following dataframe')
        print(df)

        print('OF', outFile)
        with open(outFile, 'a') as f:
            df.to_csv(f, header=False, index=False)
        f.close()
        print('[INFO]  wrote line for', dateVals[0])

        print('[INFO]  heat-deficit data loaded for',
              dateVals[0])

        print('OFR', outFileRAW)
        with open(outFileRAW, 'a') as f:
            dfRAW.to_csv(f, header=False, index=False)
        f.close()
        print('[INFO]  wrote line RAW for', dateVals[0])

    except (NameError, AttributeError, ValueError, TypeError) as e:
        print(e)
