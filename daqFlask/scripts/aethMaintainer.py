# aethMaintainer
# Run this from ../daqsite/daqFlask/scripts/
# -----------------------------------------------------------------------------
# dependencies

from datetime import datetime, timedelta
import pandas as pd
import os
import time
import sys
import shutil

from daytype import cleaner

from downloader import downloader
from heatdef import heatdef
from rechecker import aethRechecker, rawRechecker
from deadDayBuilder import deadDayBuilder

# -----------------------------------------------------------------------------


def initiate_directory(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)


def old_dir_cleanup(directory):

    now = time.time()

    expire_time = 1 * 86400

    for f in os.listdir(directory):

        if os.stat(os.path.join(directory, f)).st_mtime < now - expire_time:

            try:
                shutil.rmtree(os.path.join(directory, f))
            except NotADirectoryError:
                pass


def old_file_cleanup(directory):

    now = time.time()

    expire_time = 1 * 86400

    for f in os.listdir(directory):

        if (os.stat(os.path.join(directory, f)).st_mtime < now - expire_time) & (f.endswith(".png") | f.endswith(".zip")):

            os.remove(os.path.join(directory, f))


os.chdir('../..')
daqsite_dir = os.getcwd() + '/'

static_dir = daqsite_dir + 'daqFlask/static/'

downloads_dir = static_dir + 'downloads'
dynamic_dir = static_dir + 'dynamic/'

data_dir = daqsite_dir + 'data/'

stations_dir = data_dir + 'aeth_stations/'
holder_dir = data_dir + 'holder/'
errors_dir = data_dir + 'errors/'
daytype_dir = data_dir + 'daytype/'

aeth_errordir = errors_dir + 'aeth_errors/'
daytype_errordir = errors_dir + 'daytype_errors/'

os.chdir(daqsite_dir + '/daqFlask/scripts')


# INITIALIZATION SCRIPT?
# initiate_directory(downloads_dir)
# initiate_directory(dynamic_dir)
# initiate_directory(stations_dir)
# initiate_directory(errors_dir)
# initiate_directory(daytype_errordir)

old_dir_cleanup(holder_dir)
old_file_cleanup(dynamic_dir)
old_file_cleanup(downloads_dir)

# -----------------------------------------------------------------------------
stations = ['bv', 'ln', 'sm']
commList = ['COM6', 'COM8', 'COM4']
unitList = ['508', '507', '509']

# Uppercase list of stations
upper_stations = list(map(str.upper, stations))

# -----------------------------------------------------------------------------
# Get yesterday's date for yesterday's data

#date = '2019-04-22'
#date = datetime.strptime(date, '%Y-%m-%d').date()

date = (datetime.now() - timedelta(1)).strftime('%Y-%m-%d')
extension = str(date) + '.csv'
rawdate = datetime.strptime(date, '%Y-%m-%d')
date = datetime.strptime(date, '%Y-%m-%d').date()

# -----------------------------------------------------------------------------
# Build URLs

# For Aeth data
aeth_OutDirs = []
aeth_files = []
aeth_base_urls = []
aeth_urls = []

for i in range(len(stations)):
    aeth_OutDirs.append(stations_dir + stations[i] + '/')
    aeth_files.append(stations[i] + '-1hr-ae33-7-' + extension)
    aeth_base_urls.append(
        'http://www.airmonitoring.utah.gov/dataarchive/blackcarbon/' + stations[i] + '/')
    aeth_urls.append(aeth_base_urls[i] + aeth_files[i])

for j in range(len(stations)):
    indicator = "aeth"
    downloader(aeth_urls[j], aeth_OutDirs[j],
               aeth_files[j], indicator, aeth_errordir)

# put cleanup lines here .. check to make each AE33 csv 24 lines long, insert NaN if needed
# open each csv as aeth_urls[j] + aeth_files[j]
# check that each csv file has each hour as line
# write NaN if missing
# drop any rows after hr=23:00

#####

    file = os.path.expanduser(aeth_OutDirs[j] + aeth_files[j])
    # put the following into a try-except
    try:
        df = pd.read_csv(file, index_col=None, header=None)
        print(df)

        # drop unneeded columns from the downloaded dataframe
        endCol = int(df.shape[1])
        print('endCol', endCol)
        cols = list(range(12, endCol))
        df = df.drop(cols, axis=1)

        print('date', date)

        # check if the first line is not from the correct day
        firstDTO = datetime.strptime(df.iloc[0, 3], '%Y-%m-%d %H:%M:%S')
        if firstDTO.day != date.day:
            print('day mismatch')
            df = df.drop(0, axis=0, )
            df = df.reset_index(drop=True)

        # check if the last line is not correct
        last = len(df) - 1
        lastDTO = datetime.strptime(df.iloc[last, 3], '%Y-%m-%d %H:%M:%S')
        if lastDTO.hour != 23:
            print('hour mismatch')
            df = df.drop(last, axis=0)

        # lists for logging the missing times
        actualList = []
        neededList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                      12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]

        # run across the given dataframe
        for k in range(len(df)):

            # get hour from current line datetime stamp
            currentDTO = datetime.strptime(
                df.iloc[k, 3], '%Y-%m-%d %H:%M:%S')
            df.iloc[k, 3] = currentDTO
            actualList.append(currentDTO.hour)

        # build a list of the missing hours of the day
        missingList = list(set(neededList) - set(actualList))

        # build new lines for each missing hour, append these to the dataframe, and sort
        for m in range(len(missingList)):

            print('inside missing list worker')

            hh = missingList[m]

            newDTO = currentDTO.replace(
                hour=hh, minute=0, second=0, microsecond=0)

            newDay = datetime.strftime(newDTO, '%d-%b-%y')
            df = df.append(pd.Series([commList[j], unitList[j], newDay, newDTO, 'NaN', 'NaN',
                                      'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'], index=df.columns), ignore_index=True)
            df.sort_values(3, inplace=True)

        # TEMP sanity check
        print(df)

        with open(aeth_OutDirs[j] + aeth_files[j], 'w') as f:
            df.to_csv(f, header=False, index=False)
        f.close()

    except:
        # put a call against the empty day builder here
        print('deadDay call')
        path = aeth_OutDirs[j] + aeth_files[j]
        print('date', date)
        print('path', path)
        print('station', stations[j])
        deadDayBuilder(rawdate, path, 'AE33', stations[j])

        print()

#####

daytype_url = 'https://air.utah.gov/aq_data/forecast.action.log.csv'
daytype_file = 'forecast.action.log.csv'
daytype_modfile = daytype_dir + 'forecast.action.log[trimmed].csv'

indicator = "daytype"
downloader(daytype_url, daytype_dir, daytype_file, indicator, daytype_errordir)

full_daytype_filepath = daytype_dir + daytype_file
cleaner(full_daytype_filepath, daytype_modfile)

# Rawinsonde Data

# these lines adjust the date used for the heat deficit
HDdate = (datetime.now()).strftime('%Y-%m-%d')
rawdate = datetime.strptime(HDdate, '%Y-%m-%d')
heatdef(rawdate, data_dir)


aethRechecker(data_dir)
rawRechecker(data_dir)
