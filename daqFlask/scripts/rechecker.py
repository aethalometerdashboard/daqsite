import requests
import urllib3
import os
import csv
import pandas as pd
import sys
from datetime import datetime, timedelta

from siphon.simplewebservice.wyoming import WyomingUpperAir

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# !! the downloader needs a checker of sorts: some dates are not avail on the servers
# Earliest available dates for each station: Bountiful 20170717, Lindon 20190422, SM 20180904

# For email notification
import ssl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage


def sendemail(from_addr, to_addr, subject, message, password):

    msg = MIMEMultipart()

    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Subject'] = subject

    msg.attach(MIMEText(message, 'plain'))
    server = smtplib.SMTP('smtp.gmail.com', 587)

    server.ehlo()
    # secure our email with tls encryption
    server.starttls()
    # re-identify ourselves as an encrypted connection
    server.ehlo()

    server.login(from_addr, password)
    text = msg.as_string()
    server.sendmail(from_addr, to_addr, text)
    server.quit()


def aethRechecker(dataPath):

    aetherrordirpath = dataPath + 'errors/aeth_errors/'
    aetherrorfile = aetherrordirpath + 'errors.csv'

    if not os.path.exists(aetherrordirpath):
        os.mkdir(aetherrordirpath)

    if not os.path.exists(aetherrorfile):
        f = open(aetherrorfile, 'w+')
        f.close()

    try:
        df = pd.read_csv(aetherrorfile, header=None)
    except pd.errors.EmptyDataError:
        return

    cols = ['url', 'count']
    df.columns = cols

    indexlist = []

    for i in range(len(df)):

        r = requests.get(df['url'].iloc[i], verify=False)

        if r.status_code == 200:

            indexlist.append(i)

        else:

            df['count'].iloc[i] += 1

            if df['count'].iloc[i] >= 7:

                indexlist.append(i)

                UDC_EMAIL = os.environ.get('DAQSENDMAIL')
                UDC_PASS = os.environ.get('DAQSENDPASS')
                DAQ_EMAIL = os.environ.get('DAQRECEMAIL')

                subject = 'Missing Aethalometer Data'

                message = (
                    """The day %s has exceed it's maximum number of retries.""" % df['url'].iloc[i])

                sendemail(UDC_EMAIL, DAQ_EMAIL, subject, message, UDC_PASS)

    df.drop(df.index[indexlist], inplace=True)
    df.drop_duplicates(subset='url', keep='first', inplace=True)
    df.to_csv(aetherrorfile, header=False, index=False)


def rawRechecker(dataPath):

    station = 'SLC'

    rawerrordirpath = dataPath + '/errors/raw_errors/'

    if not os.path.exists(rawerrordirpath):
        os.mkdir(rawerrordirpath)

    rawerrorfile = rawerrordirpath + 'errors.csv'

    if not os.path.exists(rawerrorfile):
        f = open(rawerrorfile, 'w+')
        f.close()

    try:
        df = pd.read_csv(rawerrorfile, header=None)
    except pd.errors.EmptyDataError:
        return

    df = pd.read_csv(rawerrorfile, header=None)

    cols = ['date', 'count']
    df.columns = cols

    indexlist = []

    for i in range(len(df)):

        try:
            date = df['date'].iloc[i]
            date = datetime.strptime(date, '%Y-%m-%d')
            date00 = date.replace(hour=0, minute=0, second=0, microsecond=0)
            date12 = date.replace(hour=12, minute=0, second=0, microsecond=0)

            df00 = WyomingUpperAir.request_data(date00, station)
            df12 = WyomingUpperAir.request_data(date12, station)
            indexlist.append(i)

            # BEG TEMPORARY WORK
            # outFileRAW = os.path.expanduser(cwd + '/data/heatDeficitRAW.csv')
            outFileRAW = os.path.expanduser(
                cwd + '/data/heatDeficitRAWHistorical.csv')

            dfRAW = pd.concat([df00, df12], ignore_index=True)

            with open(outFileRAW, 'a') as f:
                dfRAW.to_csv(f, header=False, index=False)
            f.close()
            # END TEMPORARY WORK

            # find heat-deficit for 00-hours and 12-hours
            hd00 = df00['heatDefHeight'].sum()
            hd12 = df12['heatDefHeight'].sum()

            # build lists of date values and heat-deficit values
            dateVals = [
                date00 - timedelta(hours=7), date12 - timedelta(hours=7)]
            hdVals = [hd00, hd12]

            # build small dataframe with heat-deficit values and date values
            df = pd.DataFrame(dateVals, columns=['DATETIME'])
            df['HEATDEF'] = hdVals

            # uncomment these next two lines to clear out the heat-deficit csv
            # WARINING: this will irreversibly delete all the data inside the CSV
            # f = open(outFile, 'w+')
            # f.close()

            with open(outFile, 'a') as f:
                df.to_csv(f, header=False, index=False)
            f.close()

            print('[INFO]  heat-deficit data loaded for',
                  dateVals[0], 'and', dateVals[1])

        except ValueError:

            df['count'].iloc[i] += 1

            if df['count'].iloc[i] >= 7:

                indexlist.append(i)

                UDC_EMAIL = os.environ.get('DAQSENDMAIL')
                UDC_PASS = os.environ.get('DAQSENDPASS')
                DAQ_EMAIL = os.environ.get('DAQRECEMAIL')

                subject = 'Missing Aethalometer Data'

                message = (
                    """The day %s has exceed it's maximum number of retries.""" % df['url'].iloc[i])

                sendemail(UDC_EMAIL, DAQ_EMAIL, subject, message, UDC_PASS)

    df.drop(df.index[indexlist], inplace=True)
    df.drop_duplicates(subset='date', keep='first', inplace=True)
    df.to_csv(rawerrorfile, header=False, index=False)
