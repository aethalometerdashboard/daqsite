import pandas as pd
from siphon.simplewebservice.wyoming import WyomingUpperAir
import os
from datetime import datetime, timedelta
# from metpy.units import units
# from datetime import datetime


def rawinsonde(date, data_path):

    # declare station for call from siphon
    station = 'SLC'

    # put this inside a try statement

    try:

        # grab data from WyomingUpperAir using siphon
        print('[INFO]  grabbing data from wyoming-upper-air database')
        df = WyomingUpperAir.request_data(date, station)
        print(df.head())

        # drop excess rows .. ie, those w height > 2200
        print('[INFO]  adjusting returned database')
        rows = df.index[df['height'] >= 2200].tolist()
        idx = rows[1]
        df.drop(df.index[idx:len(df)], inplace=True)

        # drop excess columns .. ie, those not = [height, pressure, temperature]
        df.drop(['dewpoint', 'direction', 'speed', 'u_wind', 'v_wind', 'station',
                 'station_number', 'time', 'latitude', 'longitude', 'elevation'], axis=1, inplace=True)

        # create list of datetimes for the dataframe; add enough elts to make df length
        dateList = []

        for i in range(len(df)):
            dateList.append(date)

        # load the dateList as column in dataframe; convert to pandas datetime
        df['DATETIME'] = dateList
        df['DATETIME'] = pd.to_datetime(df['DATETIME'])

        # rearrange the columns
        df = df[['DATETIME', 'height', 'pressure', 'temperature']]

        # set index values for averages
        idx1 = len(df) - 2
        idx2 = len(df) - 1
        height1 = df['height'].iloc[idx1]
        height2 = df['height'].iloc[idx2]
        pres1 = df['pressure'].iloc[idx1]
        pres2 = df['pressure'].iloc[idx2]
        temp1 = df['temperature'].iloc[idx1]
        temp2 = df['temperature'].iloc[idx2]

        # get new values for pressure & temperature at 2200 by interpolation
        presNew = pres1 + ((pres2 - pres1) / (height2 - height1)
                           ) * (2200 - height1)
        tempNew = temp1 + ((temp2 - temp1) / (height2 - height1)
                           ) * (2200 - height1)

        # assign new values to the dataframe (done inplace using .at[row,col])
        df.at[idx2, 'height'] = 2200
        df.at[idx2, 'pressure'] = presNew
        df.at[idx2, 'temperature'] = tempNew

        # determine rho at height
        R = 287.06
        rhoHeight = []
        for i in range(len(df)):

            rhoHeightVal = (df['pressure'].iloc[i] * 100) / \
                ((df['temperature'].iloc[i] + 273.15) * R)

            rhoHeight.append(rhoHeightVal)

        df['rhoHeight'] = rhoHeight

        # determine potential temperature (theta) at height
        P0 = 1000
        rcp = 0.286

        thetaHeight = []
        for i in range(len(df)):

            thetaHeightVal = (df['temperature'].iloc[i] + 273.15) * \
                ((P0 / df['pressure'].iloc[i]) ** rcp)

            thetaHeight.append(thetaHeightVal)

        df['thetaHeight'] = thetaHeight

        # determine heat-deficit at height
        cp = 1005
        P0 = 1000
        factor = 0.000001

        heatDefHeight = []

        thetaCeiling = df['thetaHeight'].iloc[len(df) - 1]

        # thetaCeiling = 292.529474
        # thetaCeiling = 287.844079

        for i in range(len(df) - 1):
            rhoAvgVal = ((df['rhoHeight'].iloc[i + 1] +
                          df['rhoHeight'].iloc[i]) / 2)

            thetaAvgVal = ((df['thetaHeight'].iloc[i + 1] +
                            df['thetaHeight'].iloc[i]) / 2)

            heightDiff = df['height'].iloc[i + 1] - df['height'].iloc[i]

            heatDefHeightVal = rhoAvgVal * cp * \
                (thetaCeiling - thetaAvgVal) * heightDiff * factor

            heatDefHeight.append(heatDefHeightVal)

        # next line sets value for heat-deficit at 2200-ft
        heatDefHeight.append(0)

        # append the calculated heat-deficit values to the dataframe
        df['heatDefHeight'] = heatDefHeight

        # print()
        # sanity check; comment out or delete
        # print(df)
        # print()
        return df

    except ValueError:

        errorFilePath = data_path + "errors/raw_errors/"
        errors_filename = errorFilePath + 'errors.csv'

        date = datetime.strftime(date, '%Y-%m-%d')

        if os.path.exists(errorFilePath):

            with open(errors_filename, 'a') as f:
                f.write(date)
                f.write(',0\n')
                f.close()
        else:
            os.mkdir(errorFilePath)
            with open(errors_filename, 'a') as f:
                f.write(date)
                f.write(',0\n')
                f.close()
