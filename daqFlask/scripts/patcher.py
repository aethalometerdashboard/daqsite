from datetime import datetime, timedelta
import pandas as pd
import os
from downloader import downloader


def patcher():

    date = datetime.now() - timedelta(1)

    # TEMP date; use this line to artificially spec the date range
    date = '2019-06-03'
    date = datetime.strptime(date, '%Y-%m-%d')

    # declare dateEnd
    dateEnd = '2019-06-02'
    dateEnd = datetime.strptime(dateEnd, '%Y-%m-%d')

    os.chdir('../..')
    data_dir = os.getcwd() + "/data/HIST/"

    # build station list comm list and unit list; build uppercase list of stations
    stations = ['bv', 'ln', 'sm']
    commList = ['COM6', 'COM7', 'COM4']
    unitList = ['508', '507', '509']

    # TEMP for testing only; these lines rewrite the three above to limit testing time
    stations = ['ln']
    commList = ['COM7']
    unitList = ['507']

    upper_stations = list(map(str.upper, stations))

    while date > dateEnd:

        dateExt = date.strftime('%Y-%m-%d')
        extension = str(dateExt) + '.csv'

        # For Aeth data
        aeth_OutDirs = []
        aeth_files = []
        aeth_base_urls = []
        aeth_urls = []

        for i in range(len(stations)):
            # Aeth data
            aeth_OutDirs.append(data_dir + stations[i] + '/')
            aeth_files.append(stations[i] + '-1hr-ae33-7-' + extension)
            aeth_base_urls.append(
                'http://www.airmonitoring.utah.gov/dataarchive/blackcarbon/' + stations[i] + '/')
            aeth_urls.append(aeth_base_urls[i] + aeth_files[i])

        for j in range(len(stations)):

            # download AE33 for station; define file path; write to file path
            downloader(aeth_urls[j], aeth_OutDirs[j], aeth_files[j])
            file = os.path.expanduser(aeth_OutDirs[j] + aeth_files[j])
            df = pd.read_csv(file, index_col=None, header=None)

            # drop unneeded columns from the downloaded dataframe
            cols = list(range(12, 54))
            df = df.drop(cols, axis=1)

            print('date', date)

            # check if the first line is not from the correct day
            firstDTO = datetime.strptime(df.iloc[0, 3], '%Y-%m-%d %H:%M:%S')
            if firstDTO.day != date.day:
                print('day mismatch')
                df = df.drop(0, axis=0, )
                df = df.reset_index(drop=True)

            # check if the last line is not correct
            last = len(df) - 1
            lastDTO = datetime.strptime(df.iloc[last, 3], '%Y-%m-%d %H:%M:%S')
            if lastDTO.hour != 23:
                print('hour mismatch')
                df = df.drop(last, axis=0)

            # lists for logging the missing times
            actualList = []
            neededList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                          12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]

            # run across the given dataframe
            for k in range(len(df)):

                # get hour from current line datetime stamp
                currentDTO = datetime.strptime(
                    df.iloc[k, 3], '%Y-%m-%d %H:%M:%S')
                df.iloc[k, 3] = currentDTO
                actualList.append(currentDTO.hour)

            # build a list of the missing hours of the day
            missingList = list(set(neededList) - set(actualList))

            # build new lines for each missing hour, append these to the dataframe, and sort
            for m in range(len(missingList)):

                hh = missingList[m]

                newDTO = currentDTO.replace(
                    hour=hh, minute=0, second=0, microsecond=0)

                newDay = datetime.strftime(newDTO, '%d-%b-%y')
                df = df.append(pd.Series([commList[j], unitList[j], newDay, newDTO, 'NaN', 'NaN',
                                          'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'], index=df.columns), ignore_index=True)
                df.sort_values(3, inplace=True)

            # TEMP sanity check
            print(df)

        date = date - timedelta(1)
