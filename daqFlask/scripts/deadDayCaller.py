from deadDayBuilder import deadDayBuilder
from dateListBuilder import dateListBuilder
from datetime import datetime, timedelta

day0 = '2019-03-01'      # START DATE   (inclusive)
dayN = '2019-04-21'      # END DATE     (inclusive)
dateList = dateListBuilder(day0, dayN)


var = 'AE33'             # comment out as needed
# var = 'HD'               # comment out as needed

# loc = 'bv'
loc = 'ln'
# loc = 'sm'

pathBase = '/Users/adrian/Desktop/test/'
pathMid = '-1hr-ae33-7-'


for i in range(len(dateList)):

    date = dateList[i]
    path = pathBase + loc + pathMid + date + '.csv'

    date = datetime.strptime(date, '%Y-%m-%d')

    deadDayBuilder(date, path, var, loc)

# TESTING SECTION:: delete when working
# date = '2019-3-1'
# date = datetime.strptime(date, '%Y-%m-%d')
# path = '/Users/adrian/Desktop/bv.csv'
# var = 'AE33'
# loc = 'ln'

# deadDayBuilder(date, path, var, loc)
