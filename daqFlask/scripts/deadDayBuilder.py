from datetime import datetime, timedelta
import numpy as np
import pandas as pd
import os

# this script expects date as a datetime object
# this script expects a well formed path argument


def deadDayBuilder(date, path, var, loc):

    date = date.replace(hour=0, minute=0, second=0, microsecond=0)

    if var == 'AE33':

        # create arrays for the dataframe columns

        # create arrays of comm port and device number (24 lines each)

        comm_dict = {'bv': 'COM6', 'ln': 'COM7', 'sm': 'COM4'}
        dev_dict = {'bv': '0508', 'ln': '0507', 'sm': '0509'}

        commList = [comm_dict.get(loc) for i in range(24)]
        devList = [dev_dict.get(loc) for i in range(24)]

        # create array of date only (24 lines)
        dateSTR = date.strftime("%Y-%m-%d")
        dateList = [dateSTR for i in range(24)]

        # create array of 24 elements: one for each hour 00:00-23:00
        dtoList = []

        for i in range(24):

            dateSTR = date.strftime("%Y-%m-%d %H:%M:%S")
            dtoList.append(dateSTR)

            date = date + timedelta(hours=1)

        # create array of NaN values (24 lines each)
        n = ['NaN' for i in range(24)]

        # create dataframe from a list of arrays
        df = pd.DataFrame(
            [commList, devList, dateList, dtoList, n, n, n, n, n, n, n, n, ])

        df = df.transpose()

        print(df)

        # write out the NaN AE33 frame
        with open(path, 'a') as f:
            df.to_csv(f, header=False, index=False)
        f.close()

    elif var == 'RAW':

        print('elif')

        # build
        # df = pd.DataFrame(np.nan)

        # # write out the NaN heatdeficit frame
        # with open(path, 'a') as f:
        #     df.to_csv(f, header=False, index=False)
        # f.close()

        # write out the NaN rawinsonde frame

    else:
        print('else')
