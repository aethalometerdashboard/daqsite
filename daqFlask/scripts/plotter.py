#------------------------------------------------------------------------------
import pandas as pd
import os
from datetime import datetime, timedelta

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# print(matplotlib.get_backend())


def plotter(dc_df, hd_df, dtype_df, dc_col, hd_col, location, filename, color, linestyle, dc_label, HD_label, units, startDTO, endDTO, HD_indicator, cwd):

    timeD = endDTO - startDTO
    days = int(timeD.days) + 1
    startDTOadj = startDTO
    endDTO = endDTO + timedelta(1)

    place_dict = {'LN': 'Lindon', 'BV': 'Bountiful',
                  'SM': 'Smithfield', 'RP': 'SLC Airport'}
    locNew = place_dict[location]

    plt.rcParams['text.usetex'] = True
    plt.rcParams['figure.figsize'] = [14, 6]

    if (HD_indicator):

        fig, ax = plt.subplots()
        ax2 = ax.twinx()

        ax.plot(dc_df['DATE'], dc_df[dc_col], label=dc_label)
        ax2.plot(hd_df['DATE'], hd_df[hd_col],
                 linestyle, color=color, label=HD_label)

        ymin, ymax = ax.get_ylim()
        ymin2, ymax2 = ax2.get_ylim()

        if ymin < ymin2:
            ax.set_ylim(bottom=ymin)
            DayTypeBar_ymin = ymin
            min_indicator = True
        else:
            ax2.set_ylim(bottom=ymin2)
            DayTypeBar_ymin = ymin2
            min_indicator = False

        for i in range(len(dtype_df) - 1):
            linecolor = dtype_df.loc[dtype_df.index[i], 'DayType']

            dateline = [dtype_df.index[i], dtype_df.index[i + 1]]
            deltaline = [DayTypeBar_ymin, DayTypeBar_ymin]

            if min_indicator == True:
                line = ax.plot(dateline, deltaline,
                               color=linecolor, linewidth=12.0)
            else:
                line = ax2.plot(dateline, deltaline,
                                color=linecolor, linewidth=12.0)

        ax2.spines["right"].set_position(("axes", 1))
        ax2.yaxis.set_label_position('right')
        ax2.yaxis.set_ticks_position('right')
        ax.set_ylabel(dc_label + " " + "[ng/m$^3$]" + "      ")
        ax.set_xlabel("Date")
        ax2.set_ylabel(HD_label + " " + units + "      ")
        plt.gcf().autofmt_xdate()

        ax.grid(True)
        ax.set_title(locNew + "\n%i day(s) showing" %
                     days, loc='left', fontsize=13)
        ax.set_title("Starting: " + startDTOadj.strftime('%Y-%m-%d') + "\nEnding: " +
                     endDTO.strftime('%Y-%m-%d'), loc='right', fontsize=11, color='grey', style='italic')

        import matplotlib.patches as mpatches

        red_patch = mpatches.Patch(color='red', label='Mandatory')
        orange_patch = mpatches.Patch(color='orange', label='Voluntary')
        green_patch = mpatches.Patch(color='green', label='Unrestricted')

        leg1 = ax.legend(bbox_to_anchor=(1.05, 1.025), loc=2, borderaxespad=0.)
        leg2 = ax2.legend(bbox_to_anchor=(1.05, 0.945),
                          loc=2, borderaxespad=0.)
        leg3 = ax2.legend(handles=[red_patch, orange_patch, green_patch], title="Burn Restriction", bbox_to_anchor=(
            1.05, 0.865), loc=2, borderaxespad=0.)
        ax2.add_artist(leg2)

        final_dir = cwd + 'daqFlask'

        final_path2 = final_dir + filename

        plt.savefig(final_path2, bbox_inches='tight')

    else:

        fig, ax = plt.subplots()
        ax.plot(dc_df['DATE'], dc_df[dc_col], label=dc_label)

        ymin, ymax = ax.get_ylim()
        ax.set_ylim(bottom=ymin)
        DayTypeBar_ymin = ymin

        for i in range(len(dtype_df) - 1):
            linecolor = dtype_df.loc[dtype_df.index[i], 'DayType']

            dateline = [dtype_df.index[i], dtype_df.index[i + 1]]
            deltaline = [DayTypeBar_ymin, DayTypeBar_ymin]

            line = ax.plot(dateline, deltaline,
                           color=linecolor, linewidth=12.0)

        ax.set_ylabel(dc_label + " " + "[ng/m$^3$]" + "      ")
        ax.set_xlabel("Date")
        plt.gcf().autofmt_xdate()

        ax.grid(True)
        ax.set_title(locNew + "\n%i day(s) showing" %
                     days, loc='left', fontsize=13)
        ax.set_title("Starting: " + startDTOadj.strftime('%Y-%m-%d') + "\nEnding: " +
                     endDTO.strftime('%Y-%m-%d'), loc='right', fontsize=11, color='grey', style='italic')
        ax.legend(loc=2)

        import matplotlib.patches as mpatches

        red_patch = mpatches.Patch(color='red', label='Mandatory')
        orange_patch = mpatches.Patch(color='orange', label='Voluntary')
        green_patch = mpatches.Patch(color='green', label='Unrestricted')

        leg1 = ax.legend(handles=[red_patch, orange_patch, green_patch],  title="Burn Restriction", bbox_to_anchor=(
            1.05, 0.865), loc=2, borderaxespad=0.)

        final_dir = cwd + 'daqFlask'
        final_path2 = final_dir + filename

        plt.savefig(final_path2, bbox_inches='tight')


def compare_plotter(dc_df, hd_df, dc_col, hd_col, locations, filename, color, linestyle, dc_label, HD_label, units, startDTO, endDTO, HD_indicator, cwd):

    timeD = endDTO - startDTO
    days = int(timeD.days) + 1
    startDTOadj = startDTO
    endDTO = endDTO + timedelta(1)

    place_dict = {'LN': 'Lindon', 'BV': 'Bountiful',
                  'SM': 'Smithfield', 'RP': 'SLC Airport'}

    locNew = []
    for location in locations:
        location = location.upper()
        locNew.append(place_dict[location])

    plt.rcParams['text.usetex'] = True
    plt.rcParams['figure.figsize'] = [14, 6]

    if (HD_indicator):

        fig, ax = plt.subplots()
        ax2 = ax.twinx()

        j = 0
        for dframe in dc_df:
            ax.plot(dframe['DATE'], dframe[dc_col], label=locNew[j])
            j += 1
        ax2.plot(hd_df['DATE'], hd_df[hd_col],
                 linestyle, color=color, label=HD_label)

        ax2.spines["right"].set_position(("axes", 1))
        ax2.yaxis.set_label_position('right')
        ax2.yaxis.set_ticks_position('right')
        ax.set_xlabel("Date")
        ax.set_ylabel(dc_label + " " + "[ng/m$^3$]" + "      ")
        ax2.set_ylabel(HD_label + " " + units + "      ")
        plt.gcf().autofmt_xdate()
        ax.grid(True)

        locs = ', '.join(locNew)

        ax.set_title(locs + "\n%i day(s) showing" %
                     days, loc='left', fontsize=13)
        ax.set_title("Starting: " + startDTOadj.strftime('%Y-%m-%d') + "\nEnding: " +
                     endDTO.strftime('%Y-%m-%d'), loc='right', fontsize=11, color='grey', style='italic')
        ax.legend(loc=2)
        ax2.legend(loc=1)

        leg1 = ax.legend(bbox_to_anchor=(1.05, 1.025), loc=2, borderaxespad=0.)
        leg2 = ax2.legend(bbox_to_anchor=(1.05, 0.845),
                          loc=2, borderaxespad=0.)

        ax2.add_artist(leg2)

        final_dir = cwd + 'daqFlask'

        final_path2 = final_dir + filename

        plt.savefig(final_path2, bbox_inches='tight')

    else:
        fig, ax = plt.subplots()

        locNew = []
        for location in locations:
            location = location.upper()
            locNew.append(place_dict[location])

        j = 0
        for dframe in dc_df:
            ax.plot(dframe['DATE'], dframe[dc_col], label=locNew[j])
            j += 1

        ax.set_xlabel("Date")
        ax.set_ylabel(dc_label + " " + "[ng/m$^3$]" + "      ")
        plt.gcf().autofmt_xdate()

        ax.grid(True)

        locs = ', '.join(locNew)
        ax.set_title(locs + "\n%i day(s) showing" %
                     days, loc='left', fontsize=13)
        ax.set_title("Starting: " + startDTOadj.strftime('%Y-%m-%d') + "\nEnding: " +
                     endDTO.strftime('%Y-%m-%d'), loc='right', fontsize=11, color='grey', style='italic')
        ax.legend(loc=2)

        leg1 = ax.legend(bbox_to_anchor=(1.05, 1.025), loc=2, borderaxespad=0.)

        final_dir = cwd + 'daqFlask'

        final_path2 = final_dir + filename

        plt.savefig(final_path2, bbox_inches='tight')


def HDplotter(df_yy, dfyy_col, location, filename, color, linestyle, labelyy, units, startDTO, endDTO, cwd):

    timeD = endDTO - startDTO
    days = int(timeD.days) + 1
    startDTOadj = startDTO
    endDTO = endDTO

    plt.rcParams['text.usetex'] = True
    plt.rcParams['figure.figsize'] = [14, 6]

    place_dict = {'LN': 'Lindon', 'BV': 'Bountiful',
                  'SM': 'Smithfield', 'RP': 'SLC Airport'}
    locNew = place_dict[location]

    fig, ax = plt.subplots()

    ax.plot(df_yy['DATE'], df_yy[dfyy_col],
            linestyle, color=color, label=labelyy)

    ax.spines["right"].set_position(("axes", 1))
    ax.yaxis.set_label_position('right')
    ax.yaxis.set_ticks_position('right')
    ax.set_xlabel("Date")
    ax.set_ylabel(labelyy + " " + units + "      ")
    plt.gcf().autofmt_xdate()
    ax.grid(True)
    ax.set_title(locNew + "\n%i day(s) showing" %
                 days, loc='left', fontsize=13)
    ax.set_title("Starting: " + startDTOadj.strftime('%Y-%m-%d') + "\nEnding: " +
                 endDTO.strftime('%Y-%m-%d'), loc='right', fontsize=11, color='grey', style='italic')
    ax.legend(loc=2)

    final_dir = cwd + 'daqFlask'

    final_path2 = final_dir + filename

    plt.savefig(final_path2, bbox_inches='tight')
