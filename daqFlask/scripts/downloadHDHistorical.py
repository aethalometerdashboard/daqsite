from heatdef import heatdef
from datetime import datetime, timedelta
import pandas as pd
import os
import time

# set the time delay to 5 for large downloads
start_time = time.time()
timeDelay = 1
totalDays = 9
startDate = '2013-3-20'

date = datetime.strptime(startDate, '%Y-%m-%d')
dateEnd = date + timedelta(totalDays)
count = 0
while date < dateEnd:

    heatdef(date)
    date = date + timedelta(1)
    time.sleep(timeDelay)
    count += 1

count = count * 2
elapsed_time = time.time() - start_time
timePer = elapsed_time / count

print('elapsed time:' + str(elapsed_time))
print('server query ct: ' + str(count))
print('time per DL: ' + str(timePer))
