import pandas as pd
from datetime import datetime, timedelta
import numpy as np

pd.options.mode.chained_assignment = None  # default='warn'


def averageDelta(frame, idx):

    print('entered averageDelta fxn')

    outFrame = pd.DataFrame(np.nan, index=idx, columns=(
        'DATE', 'delta24', 'delta24minus'))

    # outFrame = pd.DataFrame(index=idx, columns=(
    #     'DATE', 'delta24', 'delta24minus'))
    # outFrame = outFrame.fillna(0)

    print('outframe follows')
    print()
    print(outFrame)
    print()
    i = 0
    j = 0

    while i < len(frame):

        # grab date for active day (the i-th row) (this is a single date)
        dateTemp = frame['DATE'].iloc[i]
        #dateTempDTO = datetime.strptime(dateTemp, '%d-%m-%Y')
        # print(dateTempDTO)
        # print(type(dateTempDTO))
        print('dateTemp:', dateTemp)

        # grab all rows with active date as slice (this is a 24 line frame)
        dataTemp = frame[frame['DATE'] == dateTemp]
        print('dataTemp:', dataTemp)
        count = dataTemp['CH1'].isna().sum()
        print('count', count)

        # write active date to j-th row of outframe
        outFrame['DATE'].iloc[j] = dateTemp
        print('outFrame date at iloc:', outFrame['DATE'].iloc[j])

        # write 24-hr average of slice to j-th row of outframe
        if count != 0:
            outFrame['delta24'].iloc[j] = abs((
                dataTemp['delta'].sum()) / (24 - count))
            print('outFrame delta avgd', outFrame['delta24'].iloc[j])

        else:
            outFrame['delta24'].iloc[j] = abs((dataTemp['delta'].sum()) / 24)
            print('outFrame delta avgd', outFrame['delta24'].iloc[j])

        # not certain we need the delta24minus column

        if i > 0:
            dateTemp = frame['DATE'].iloc[i - 1]
            dataTemp = frame[frame['DATE'] == dateTemp]
            outFrame['delta24minus'].iloc[j] = outFrame['delta24'].iloc[j] - \
                dataTemp['delta'].iloc[23]

        i = i + 24
        j = j + 1

        print('new outframe')
        print(outFrame)
        print()

    outFrame['DATE'] = pd.to_datetime(outFrame['DATE'])
    outFrame['DATE'] += pd.to_timedelta(12, unit='h')
    print('exit averageDelta fxn')
    return outFrame
