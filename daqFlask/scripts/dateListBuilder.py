from datetime import datetime, timedelta


def dateListBuilder(dateStart, dateEnd):

    dateList = []

    #dateStart = '2019-3-1'
    date = datetime.strptime(dateStart, '%Y-%m-%d')
    #dateEnd = '2019-3-12'
    dateEnd = datetime.strptime(dateEnd, '%Y-%m-%d')

    dateSTR = date.strftime("%Y-%m-%d")
    dateList.append(dateSTR)

    while date < dateEnd:

        date = date + timedelta(1)
        dateSTR = date.strftime("%Y-%m-%d")
        dateList.append(dateSTR)

    # print(dateList)

    return dateList
