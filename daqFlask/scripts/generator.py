# LIBRARIES: dependencies are loaded
import pandas as pd
import os
from datetime import datetime, timedelta
from shutil import copyfile

from daqFlask.scripts.average import averageDelta
from daqFlask.scripts.plotter import plotter, compare_plotter, HDplotter
from daqFlask.scripts.daytype import selector

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


def initiate_directory(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)


def generator(startDate, endDate, stations, user, HD_indicator, SP_indicator, root_path):

    # ------------------------------------------------------------------------------
    startDTO = datetime.strptime(startDate, '%Y-%m-%d')
    startDTO = startDTO - timedelta(1)
    startDTOadj = startDTO + timedelta(1)

    endDTO = datetime.strptime(endDate, '%Y-%m-%d')
    endDTO_DT = endDTO
    endDTO = endDTO - timedelta(1)

    timeD = endDTO - startDTO
    days = int(timeD.days)
    print(str(days) + ' days of data will be loaded into analysis.csv')

    # sets the initial date for the loop
    dateDTO = endDTO

    # sets the path for the temporary csv data; clears csv contents

    data_dir = root_path + 'data/'
    holderPath = data_dir + 'holder/'

    userPath = holderPath + user + '/'

    initiate_directory(holderPath)
    initiate_directory(userPath)

    if (stations == []) & (HD_indicator):

        file_names = []

        dataHDFixed24Path = os.path.expanduser(
            data_dir + 'heatdeficit/heatDeficit.csv')
        dataHDFixed24 = pd.read_csv(dataHDFixed24Path, header=0)
        cols = ['DATE', 'HD24']
        dataHDFixed24.columns = cols
        dataHDFixed24['DATE'] = pd.to_datetime(dataHDFixed24['DATE'])

        # -----------------------------------------------------------------------------
        # TRIM HD-CORRECTED FOR USE IN PLOTTERS

        startDTOtemp = startDTO + \
            timedelta(hours=24) + timedelta(hours=5)
        startIdxVal = dataHDFixed24[dataHDFixed24.DATE ==
                                    startDTOtemp].first_valid_index()
        endDTOtemp = endDTO + timedelta(hours=17)
        endIdxVal = dataHDFixed24[dataHDFixed24.DATE ==
                                  endDTOtemp].first_valid_index()

        dataHDFixed24Trim = dataHDFixed24[startIdxVal:endIdxVal + 1]

        dataPath = userPath + user + '.csv'
        f = open(dataPath, 'w+')
        f.close()

        dataPathHD = os.path.expanduser(
            userPath + 'HD' + user + '.csv')

        f = open(dataPathHD, 'w+')
        f.close()

        with open(dataPathHD, 'a') as f:
            dataHDFixed24Trim.to_csv(f, header=True, index=False)
        f.close()

        # DAY TYPE STUFF

        location = 'rp'

        upper_location = location.upper()

        root = 'dynamic/plot_' + upper_location + '_HD24' + user + '.png'
        file_name = '/static/' + root

        file_names.append(root)

        # PLOTTING
        HDplotter(dataHDFixed24Trim, 'HD24', upper_location,
                  file_name, 'C4', '--', "Heat Deficit", "[MJ/m$^2$]", startDTOadj, endDTO, root_path)

        return file_names

    else:

        dataPath = userPath + user + '.csv'
        f = open(dataPath, 'w+')
        f.close()

        copyfile(root_path + 'daqFlask/static/ReadMe.txt',
                 userPath + 'ReadMe.txt')

        # ------------------------------------------------------------------------------

        for j in range(len(stations)):

            for i in range(days):

                # create a path for the dated file we will fetch
                dateExtension = dateDTO.strftime('%Y-%m-%d') + str('.csv')
                pathBase = data_dir + 'aeth_stations/' + \
                    stations[j] + '/' + stations[j] + \
                    '-1hr-ae33-7-' + dateExtension
                datePath = os.path.expanduser(pathBase)

                # fetch the targeted csv file and read into a dataframe
                try:
                    colList = [x for x in range(0, 11)]
                    dateFrame = pd.read_csv(
                        datePath, index_col=0, header=None, usecols=colList)
                except FileNotFoundError:
                    dateFrame = pd.DataFrame()
                    print("No Data for " +
                          stations[j] + ' for date ' + dateExtension)

                # append this data into the analysis.csv file
                with open(dataPath, 'a') as f:
                    (dateFrame).to_csv(f, header=False)
                f.close()

                # adjust date to one day prior for next loop iter
                dateDTO = dateDTO - timedelta(1)

            dateDTO = endDTO

        # ------------------------------------------------------------------------------
        # load the csv-stored data into a pandas frame called data

        data = pd.read_csv(dataPath, index_col=0, header=None)

        # trim unnecessary columns off; insert header with named values
        data = data.iloc[:, 0:10]
        cols = ['UNIT', 'DATE', 'DATETIME', 'CH1', 'CH2',
                'CH3', 'CH4', 'CH5', 'CH6', 'CH7']
        data.columns = cols

        # convert column 3 of the data df to a datetime type; can check the datatypes of each col
        data['DATETIME'] = pd.to_datetime(data['DATETIME'])

        # ------------------------------------------------------------------------------
        # DATA CONSTRUCTOR: Delta Carbon & Temperature Deficit
        # find brown carbon count .. this is delta of columns 8,3
        data['delta'] = (data['CH1'] - data['CH6']) * 1000

        data = data.sort_values('DATETIME')

        with open(dataPath, 'w') as f:
            (data).to_csv(f, header=True)
        f.close()

        #    with open(dataPath, 'a') as f:
        #        (data).to_csv(f, header=False)
        #    f.close()

        # ------------------------------------------------------------------------------
        # build three unique sets (split across 0507, 0508, 0509 .. column 1)
        # these sets contain the brown carbon data for each of three monitoring stations

        delta_dict = {'bv': 508, 'rp': 59, 'ln': 507, 'sm': 509}

        delta_list = []
        for station in stations:

            delta_loc = data[data['UNIT'] == delta_dict[station]]
            delta_loc = delta_loc.sort_values('DATETIME', ascending=True)
            delta_list.append(delta_loc)

        # ------------------------------------------------------------------------------
        # find average brown carbon over 24 hr periods beginning at midnight
        dc_loc_list = []
        avg_delta_list = []

        idx = pd.date_range(startDTO + timedelta(hours=24),
                            periods=days, freq='D')
        for delta in delta_list:
            avg_delta_list.append(averageDelta(delta, idx))

        # -----------------------------------------------------------------------------
        # 20190407 load corrected heat deficit data
        # this is hard coded HD data for the period of the study
        # this section needs to be replaced with HD data called from stuffs

        dataHDFixed24Path = os.path.expanduser(
            root_path + '/data/heatdeficit/heatDeficit.csv')
        dataHDFixed24 = pd.read_csv(dataHDFixed24Path, header=0)
        cols = ['DATE', 'HD24']
        dataHDFixed24.columns = cols
        dataHDFixed24['DATE'] = pd.to_datetime(dataHDFixed24['DATE'])

        # -----------------------------------------------------------------------------
        # TRIM HD-CORRECTED FOR USE IN PLOTTERS

        startDTOtemp = startDTO + timedelta(hours=5) + timedelta(hours=24)
        startIdxVal = dataHDFixed24[dataHDFixed24.DATE ==
                                    startDTOtemp].first_valid_index()
        endDTOtemp = endDTO + timedelta(hours=17)
        endIdxVal = dataHDFixed24[dataHDFixed24.DATE ==
                                  endDTOtemp].first_valid_index()

        dataHDFixed24Trim = dataHDFixed24[startIdxVal:endIdxVal + 1]

        dataHDFixed24Trim = dataHDFixed24Trim.round(2)

        dataPathHD = userPath + 'HD' + user + '.csv'

        with open(dataPathHD, 'a') as f:
            dataHDFixed24Trim.to_csv(f, header=True, index=False)
        f.close()

        # -----------------------------------------------------------------------------
        # PLOTTER CALLS FOR AETH vs HEAT-DEFICIT

        file_names = []

        DTpath = data_dir + '/daytype/forecast.action.log[trimmed].csv'

        if ((SP_indicator) | (len(stations) <= 1)):

            for j in range(len(stations)):

                location = stations[j]
                DayTypeDF = selector(DTpath, startDTOadj, endDTO_DT, location)

                upper_location = location.upper()

                single_plot_path = 'dynamic/plot_' + upper_location + '_AE24-HD24' + user + '.png'
                file_name = '/static/' + single_plot_path

                file_names.append(single_plot_path)

                plotter(avg_delta_list[j], dataHDFixed24Trim, DayTypeDF, 'delta24', 'HD24', upper_location,
                        file_name, 'C4', '--', "Delta-C (24 Hour Average)", "Heat Deficit", "[MJ/m$^2$]", startDTOadj, endDTO, HD_indicator, root_path)
        else:

            combined_plot_path = 'dynamic/plot_combined' + '_AE24-HD24' + user + '.png'
            file_name_combined = '/static/' + combined_plot_path
            file_names.append(combined_plot_path)

            compare_plotter(avg_delta_list, dataHDFixed24Trim, 'delta24', 'HD24', stations,
                            file_name_combined, 'C4', '--', "Delta-C (24 Hour Average)", "Heat Deficit", "[MJ/m$^2$]", startDTOadj, endDTO, HD_indicator, root_path)

        return file_names
