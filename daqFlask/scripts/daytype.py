import pandas as pd
from datetime import datetime, timedelta
import os


def selector(path, startDate, endDate, location):

    place_dict = {'ln': 'utah', 'bv': 'bv', 'sm': 'cache', 'rp': 'slc'}

    location = place_dict[location]

    DayTypeDF = pd.read_csv(path)

    DayTypeDF = DayTypeDF[(DayTypeDF['Location'] == location)]

    DayTypeDF = DayTypeDF.iloc[:, [2, 3]]

    DayTypeDF['Date'] = pd.to_datetime(DayTypeDF['Date'])

    DayTypeDF.drop_duplicates(subset=['Date'], keep='last', inplace=True)

    DayTypeDF.set_index('Date', inplace=True)

    DayTypeDF = DayTypeDF[startDate:endDate]

    return DayTypeDF


def cleaner(daytype_file, daytype_modfile):

    DayTypeDF = pd.read_csv(daytype_file, header=None)

    DayTypeDF = DayTypeDF.iloc[:, [1, 2, 3]]
    colnames = ['Location', 'Date', 'DayType']
    DayTypeDF.columns = colnames

    DayTypeDF = DayTypeDF.replace(
        ['good', 'moderate', 'unhealthyforsensitivegroups', 'unhealthy'], ['g', 'orange', 'r', 'r'])

    DayTypeDF.to_csv(daytype_modfile)
