from heatdef import heatdef
from datetime import datetime, timedelta
import pandas as pd
import os
import time

os.system('cls' if os.name == 'nt' else 'clear')

# ----- User Input Section -------------------------

print('-- HeatDeficit Mass Downloader --')
print()
print('* this tool will download heat-deficit data beginning')
print('  with the START-DATE for X number of total days.')
print()
print('  a time-delay, N(seconds), specifies the interval btwn')
print('  queries to the wyoming upper-air api')
print()
print('  results append to csv file at \{pick-any-path\}/data/')
print('  (build a directory called data if one does not exist)')
print('  (if no csv exists, one will be created.)')
print('')
print('-------------------------------------------------------')
print('  you will now be prompted to supply:')
print('  start-date, total-days, time-delay, output-path')
print()
startDate = input('  Specify Start Date in format YYYY-MM-DD: ')
totalDays = input('           Specify Total Days To Download: ')
totalDays = int(totalDays)
print()
timeDelay = input('                   Specify Time Delay (s): ')
timeDelay = int(timeDelay)
print()
print('You will now enter a path for the output file.')
print()
print('Note: the script expects an existing folder named data.')
print('      i.e., a valid input path is: ~/Desktop/data/')
print()
outpath = input('                      Specify Output Path: ')
print()
print('... ... ... ...')
print('Note:  for each api call multiple lines will print.')
print()
print('       interrupt this process by pressing:  CTRL+C')
print()
print('       larger total-days --> process takes longer')
print()
print('       lots of text can print to this console window')
time.sleep(10)
print('<<< BEGIN HD Historical Building >>>')
time.sleep(1)
print('')

print

# ----- Active Script Section -----------------------
start_time = time.time()

#startDate = '2018-11-19'
#totalDays = 273

#outpath = '/Users/Alex/Desktop/data/'
#timeDelay = 2

date = datetime.strptime(startDate, '%Y-%m-%d')
dateEnd = date + timedelta(totalDays)
count = 0

while date < dateEnd:

    heatdef(date, outpath)
    date = date + timedelta(1)
    time.sleep(timeDelay)
    count += 1

count = count * 2
elapsed_time = time.time() - start_time
timePer = elapsed_time / count

print('elapsed time:' + str(elapsed_time))
print('server query ct: ' + str(count))
print('time per DL: ' + str(timePer))


# ----- working area follows


# outfile = input('  Specify File Name: ') # this does not exist yet as part of the input argument to heatDef (consider adding it)


# ----- working area ends
